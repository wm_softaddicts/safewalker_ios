//
//  NSString+AESCrypt.h
//
//  AES128Encryption + Base64Encoding
//

#import "NSString+AESCrypt.h"

@implementation NSString (AESCrypt)

- (NSString *)AES128EncryptWithKey:(NSString *)key
{
    
    NSString *strToEncript = self;
    
    while (([strToEncript length] % 16) != 0) {
        strToEncript = [NSString stringWithFormat:@"%@ ", strToEncript];
    }
    
    NSData *plainData = [strToEncript dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptedData = [plainData AES128EncryptWithKey:key];
    
    NSString *encryptedString = [encryptedData base64EncodedStringWithOptions:0];
    
    NSData *nsdata = [encryptedString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    //NSString *encryptedString = [encryptedData base64Encoding];
    
    return base64Encoded;
}

@end
