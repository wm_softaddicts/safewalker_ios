@interface NSDictionary (Additions)

- (id)objectForKeyOrNil:(id)aKey;
- (NSString *)jsonString;

@end
