#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (id)objectForKeyOrNil:(id)aKey {
    id obj = [self objectForKey:aKey];
    
    if (obj == (id)[NSNull null])
        return nil;
    
    return obj;
}

- (NSString *)jsonString {
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        return @"";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }

}

@end
