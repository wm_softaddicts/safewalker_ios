//
//  Const.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 17.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#ifndef SafeWalker_Const_h
#define SafeWalker_Const_h

#import "SWConnectManager.h"
#import "SWDataManager.h"
#import "SWLocationManager.h"
#import "NSString+AESCrypt.h"

#define SW_SERVER_URL   @"http://safewalker.softaddicts.com/mobile/"

#define CManager    [SWConnectManager instance]
#define DManager    [SWDataManager instance]
#define LManager    [SWLocationManager instance]

#define LS(S)   [[SWDataManager instance] textForKey:S]

#define encryptKey  @"Bar12345Bar12345"

#define ES(D)   [[D jsonString] AES128EncryptWithKey:encryptKey]

#endif
