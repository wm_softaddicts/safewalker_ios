//
//  Utils.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 17.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (BOOL)validateEmail:(NSString *)candidate {
    NSString*emailRegex =@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate*emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return[emailTest evaluateWithObject:candidate];
}

+ (NSString *)encodeString:(NSString *)string {
    
    NSString *encodedString = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return encodedString;
}

+ (NSString *)urlEncode:(NSString *)str {
    
    NSString *result = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
    return result;
    
}

+ (BOOL)isiPhone {
    
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
    
}

@end
