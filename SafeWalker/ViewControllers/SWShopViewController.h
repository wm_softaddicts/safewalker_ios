//
//  SWShopViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"

@interface SWShopViewController : SWViewController {
    
    BOOL loadingInProgress;
    
}


@property (nonatomic, strong) SWCoupon *selectedCoupon;

@property (nonatomic, weak) IBOutlet UITableView *tableViewShop;

@property (nonatomic, weak) IBOutlet UIView *viewInfo;
@property (nonatomic, weak) IBOutlet UIView *viewInfoContentBuy;

@property (nonatomic, weak) IBOutlet UILabel *labelInfoTitleBuy;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoExpiresBuy;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoPriceBuy;

@property (nonatomic, weak) IBOutlet UIView *viewInfoRounded;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewInfoContent;
@property (nonatomic, weak) IBOutlet UIView *viewInfoContent;

@property (nonatomic, weak) IBOutlet UILabel *labelInfoTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imageInfo;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoExpires;
@property (nonatomic, weak) IBOutlet UIButton *buttonBuy;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoDescription;

@end
