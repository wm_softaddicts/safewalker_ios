//
//  SWPasswordViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWPasswordViewController.h"

@interface SWPasswordViewController ()

@end

@implementation SWPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _viewSuccess.alpha = 0.0;
    
}

#pragma mark - UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _textFieldOldPassword) {
        [_textFieldPassword becomeFirstResponder];
    } else if (textField == _textFieldPassword) {
        [_textFieldConfirmPassword becomeFirstResponder];
    } else {
        [self buttonChangePasswordPressed:nil];
    }
    
    return YES;
    
}

- (void)closeKeyboard {
    
    [_textFieldOldPassword resignFirstResponder];
    [_textFieldPassword resignFirstResponder];
    [_textFieldConfirmPassword resignFirstResponder];
    
}

- (IBAction)buttonChangePasswordPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    _viewSuccess.alpha = 0.0;
    
    NSString *strError = @"";
    
    if ([_textFieldOldPassword.text length] == 0 || [_textFieldPassword.text length] == 0 || [_textFieldConfirmPassword.text length] == 0) {
        
        strError = LS(@"Please fill all field");
        
    }
    
    if ([strError length] == 0 && ![_textFieldPassword.text isEqualToString:_textFieldConfirmPassword.text]) {
        
        strError = LS(@"Incorrect password");
        
    }
    
    if ([strError length] == 0) {
        
        [self showHUD];
        
        [CManager changePassword:_textFieldPassword.text password:_textFieldOldPassword.text completionBlock:^(NSDictionary *result) {
            
            [self hideHUD];
            
            if (!result || [result objectForKeyOrNil:@"error"]) {
                
                if ([result objectForKey:@"error"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                    [alert show];
                }
                
            } else {
                
                
                _labelSuccess.text = LS(@"Your password has been changed successfuly");
                
                [UIView animateWithDuration:0.3 animations:^{
                    _viewSuccess.alpha = 1.0;
                }];
                
                
                //[self.navigationController popViewControllerAnimated:YES];
                
            }
            
            
        }];
        
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strError message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
        [alert show];
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
