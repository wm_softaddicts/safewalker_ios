//
//  SWRegisterViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 11.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWRegisterViewController.h"
#import "SWMainMenuViewController.h"

@interface SWRegisterViewController ()

@end

@implementation SWRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _textFieldEmail) {
        [_textFieldPassword becomeFirstResponder];
    } else if (textField == _textFieldPassword) {
        [_textFieldConfirmPassword becomeFirstResponder];
    } else {
        [self buttonRegisterPressed:nil];
    }
    
    return YES;
    
}

- (void)closeKeyboard {
    
    [_textFieldEmail resignFirstResponder];
    [_textFieldPassword resignFirstResponder];
    [_textFieldConfirmPassword resignFirstResponder];
    
}

- (IBAction)buttonRegisterPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    
    NSString *strError = @"";
    
    if ([_textFieldEmail.text length] == 0 || [_textFieldPassword.text length] == 0 || [_textFieldConfirmPassword.text length] == 0) {
        
        strError = LS(@"Please fill all field");
        
    }
    
    if ([strError length] == 0 && ![Utils validateEmail:_textFieldEmail.text]) {
        
        strError = LS(@"Incorrect email format");
        
    }
    
    if ([strError length] == 0 && ![_textFieldPassword.text isEqualToString:_textFieldConfirmPassword.text]) {
        
        strError = LS(@"Incorrect password");
        
    }
    
    if ([strError length] == 0) {
        
        [self showHUD];

        [CManager signup:_textFieldEmail.text password:_textFieldPassword.text completionBlock:^(NSDictionary *result) {
            
            [self hideHUD];
            
            if (!result || [result objectForKeyOrNil:@"error"]) {
                
                if ([result objectForKey:@"error"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                    [alert show];
                }
                
            } else if (DManager.user_token) {
                
                SWMainMenuViewController *controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"SWMainMenuViewController"];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
        }];
        
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strError message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
        [alert show];
        
    }
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
