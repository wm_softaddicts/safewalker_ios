//
//  SWPasswordViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"

@interface SWPasswordViewController : SWViewController

@property (nonatomic, weak) IBOutlet UITextField *textFieldOldPassword;
@property (nonatomic, weak) IBOutlet UITextField *textFieldPassword;
@property (nonatomic, weak) IBOutlet UITextField *textFieldConfirmPassword;

@property (nonatomic, weak) IBOutlet UIView *viewSuccess;
@property (nonatomic, weak) IBOutlet UILabel *labelSuccess;

@end
