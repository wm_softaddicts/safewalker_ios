//
//  SWRewardsViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWRewardsViewController.h"
#import "SWPurchasesTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SWRewardsViewController ()

@end

@implementation SWRewardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _viewInfo.alpha = 0.0;
    _viewInfoRounded.layer.cornerRadius = 10;
    _viewInfoRounded.layer.masksToBounds = YES;

    _tableViewRewards.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableViewRewards.frame.size.width, 16.0)];
    _tableViewRewards.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableViewRewards.frame.size.width, 4.0)];
    
    _labelNoData.hidden = YES;
    
    [self buttonSegmentPressed:[[_viewSegmentsBar subviews] objectAtIndex:0]];
    
}

#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [DManager.rewards count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId = @"SWPurchasesTableViewCell";
    SWPurchasesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SWPurchasesTableViewCell" owner:tableView options:nil] objectAtIndex:([Utils isiPhone] ? 0 : 1)];
    }
    
    SWCoupon *coupon = [DManager.rewards objectAtIndex:indexPath.row];
    
    cell.imgCoupon.image = nil;
    
    if (coupon.img) [cell.imgCoupon sd_setImageWithURL:[NSURL URLWithString:coupon.img]];
    
    cell.labelTitle.text = coupon.title;
    NSString *status = LS(@"Expires");
    if (selectedType == 1) status = LS(@"Redeemed");
    if (selectedType == 2) status = LS(@"Expired");
    
    cell.labelExpire.text = [NSString stringWithFormat:@"%@ %@", status, coupon.expire_date];
    
    cell.showSelection = (selectedType < 2);
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    cell.selectedBackgroundView = [UIView new];
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (selectedType < 2) {
    
        SWCoupon *coupon = [DManager.rewards objectAtIndex:indexPath.row];
        
        if (_selectedCoupon && ([_selectedCoupon.id isEqualToString:coupon.id])) {
            [self showCouponInfo];
        } else {
            
            [self showHUD];
            
            [CManager getCoupon:coupon.id completionBlock:^(NSDictionary *result) {
               
                _selectedCoupon = [SWCoupon couponWithDictionary:result];
                
                [self hideHUD];
                [self showCouponInfo];
                
            }];
            
        }
    }
    
}

- (void)showCouponInfo {
    
    _imageInfo.image = nil;
    
    if (_selectedCoupon.img) [_imageInfo sd_setImageWithURL:[NSURL URLWithString:_selectedCoupon.img]];
    
    _labelInfoTitle.text = _selectedCoupon.title;
    _labelInfoExpires.text = [NSString stringWithFormat:@"%@: %@", LS(@"Expires"), _selectedCoupon.expire_date];
    int titleWidth = [LS(@"Expires") length] + 2;
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:_labelInfoExpires.text];
    
    [attrStr addAttribute:NSForegroundColorAttributeName value:_labelInfoExpires.textColor range:NSMakeRange(0, attrStr.length)];
    [attrStr addAttribute:NSFontAttributeName value:_labelInfoExpires.font range:NSMakeRange(0, attrStr.length)];
    
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.0 green:0.56 blue:0.0 alpha:1.0] range:NSMakeRange(titleWidth, [_labelInfoExpires.text length] - titleWidth)];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:_labelInfoExpires.font.pointSize] range:NSMakeRange(titleWidth, [_labelInfoExpires.text length] - titleWidth)];
    
    _labelInfoExpires.attributedText = attrStr;
    
    _labelInfoVoucher.text = [NSString stringWithFormat:@"%@: %@", LS(@"Voucher"), _selectedCoupon.number];
    
    titleWidth = [LS(@"Voucher") length] + 2;
    
    attrStr = [[NSMutableAttributedString alloc] initWithString:_labelInfoVoucher.text];
    
    [attrStr addAttribute:NSForegroundColorAttributeName value:_labelInfoVoucher.textColor range:NSMakeRange(0, attrStr.length)];
    [attrStr addAttribute:NSFontAttributeName value:_labelInfoVoucher.font range:NSMakeRange(0, attrStr.length)];
    
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:_labelInfoVoucher.font.pointSize] range:NSMakeRange(titleWidth, [_labelInfoVoucher.text length] - titleWidth)];
    
    _labelInfoVoucher.attributedText = attrStr;
    
    _labelInfoBarCode.text = [NSString stringWithFormat:@"*%@*", _selectedCoupon.number];
    _labelInfoBarCode.font = [UIFont fontWithName:@"3Of9Barcode" size:48.0];

    _buttonRedeem.enabled = (selectedType == 0);
    _buttonCancel.hidden = !_buttonRedeem.enabled;
    
    
    
    _labelInfoDescription.text = _selectedCoupon.couponDescription;

    NSDictionary *attributes = @{ NSFontAttributeName: _labelInfoDescription.font };
    
    CGRect size = [_labelInfoDescription.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(_labelInfoDescription.frame), MAXFLOAT)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil];
    _labelInfoDescription.frame = CGRectMake(_labelInfoDescription.frame.origin.x, _buttonRedeem.frame.origin.y + ([Utils isiPhone] ? 55.0 : 120.0) * (_buttonCancel.hidden ? 1 : 2), _labelInfoDescription.frame.size.width, size.size.height + 5.0);
    _viewInfoContent.frame = CGRectMake(0, 0, _viewInfoContent.frame.size.width, _labelInfoDescription.frame.origin.y + _labelInfoDescription.frame.size.height + 10.0);
    _scrollViewInfoContent.contentSize = _viewInfoContent.frame.size;
    _scrollViewInfoContent.contentOffset = CGPointZero;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewInfo.alpha = 1.0;
        
    }];
    
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ((scrollView == _tableViewRewards) && !loadingInProgress && (DManager.rewardsPage < DManager.rewardsPages)) {
        
        float pageHeight = 20 * _tableViewRewards.rowHeight;
        
        float maxOffset = (DManager.rewardsPage - 1) * pageHeight + (pageHeight / 2);
        
        if (scrollView.contentOffset.y > maxOffset) {
            loadingInProgress = YES;
            
            NSString *type = @"purchased";
            if (selectedType == 1) type = @"used";
            if (selectedType == 2) type = @"expired";
            
            [CManager updateRewards:type page:DManager.rewardsPage completionBlock:^(NSDictionary *result) {
                
                DManager.rewardsPage++;
                
                if ([result objectForKeyOrNil:@"coupons"]) {
                    for (NSDictionary *coupon in [result objectForKey:@"coupons"]) {
                        
                        [DManager.rewards addObject:[SWCoupon couponWithDictionary:coupon]];
                    }
                }
                
                [_tableViewRewards reloadData];
                loadingInProgress = NO;
                
            }];
            
        }
        
    }
    
}


#pragma mark - Buttons

- (IBAction)buttonSegmentPressed:(UIButton *)button {
    
    _labelNoData.hidden = YES;
    
    for (UIButton *buttonSegment in [_viewSegmentsBar subviews]) {
        buttonSegment.enabled = (buttonSegment.tag != button.tag);
    }
    selectedType = button.tag;
    
    [self showHUD];
    
    NSString *type = @"purchased";
    if (button.tag == 1) type = @"used";
    if (button.tag == 2) type = @"expired";
    
    DManager.rewardsPages = 0;
    DManager.rewardsPage = 0;
    
    [CManager updateRewards:type page:DManager.rewardsPage completionBlock:^(NSDictionary *result) {
        
        DManager.rewardsPage++;
        DManager.rewardsPages = [[result objectForKeyOrNil:@"pages"] integerValue];
        
        [DManager.rewards removeAllObjects];
        
        if ([result objectForKeyOrNil:@"coupons"]) {
            for (NSDictionary *coupon in [result objectForKey:@"coupons"]) {
                
                [DManager.rewards addObject:[SWCoupon couponWithDictionary:coupon]];
            }
        }
        
        [self hideHUD];
        
        if ([DManager.rewards count] == 0) {
            
            NSArray *texts = [NSArray arrayWithObjects:@"You don't have rewards", @"You don't have redemed rewards", @"You don't have expired rewards", nil];
            
            _labelNoData.text = [texts objectAtIndex:button.tag];
            _labelNoData.hidden = NO;
            
        }
        
        [_tableViewRewards reloadData];
        [_tableViewRewards scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewInfo.alpha = 0.0;
            
        }];

    }];

    
}

- (IBAction)buttonRedeemPressed:(UIButton *)button {
    
    if (_selectedCoupon) {
        
        [self showHUD];
        
        [CManager userRedeem:_selectedCoupon.id completionBlock:^(NSDictionary *result) {
            
            if (!result || [result objectForKeyOrNil:@"error"]) {
                
                [self hideHUD];
                
                if ([result objectForKey:@"error"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                    [alert show];
                }
                
            } else {
                
                [self buttonSegmentPressed:[[_viewSegmentsBar subviews] objectAtIndex:0]];

            }
            
        }];
        
        
    }
    
}

- (IBAction)buttonBackPressed:(UIButton *)button {
    
    if (_viewInfo.alpha > 0) {
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewInfo.alpha = 0.0;
            
        }];
        
    } else {
    
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
