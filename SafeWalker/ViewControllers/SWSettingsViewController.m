//
//  SWSettingsViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 15.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWSettingsViewController.h"

@interface SWSettingsViewController ()

@end

@implementation SWSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buttons

- (IBAction)buttonFeedbackPressed:(UIButton *)button {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *email = [[MFMailComposeViewController alloc] init];
        email.mailComposeDelegate = self;
        
        if ([DManager.profile objectForKeyOrNil:@"feedback_email"]) {
            [email setToRecipients:[NSArray arrayWithObject:[DManager.profile objectForKey:@"feedback_email"]]];
        }
        
        [email setSubject:LS(@"SafeWalker")];
        
        [self presentViewController:email animated:YES completion:nil];
        
    } else {
        UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:LS(@"Email Failure") message:LS(@"Your device is not configured to send email") delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles: nil];
        [emailAlert show];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)buttonLogOutPressed:(UIButton *)button {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"Are You Sure?")
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:LS(@"Cancel")
                                              otherButtonTitles:LS(@"Ok"), nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if (buttonIndex == 1) {
            
            [CManager logout];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        
        
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
