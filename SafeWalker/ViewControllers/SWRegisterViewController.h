//
//  SWRegisterViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 11.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"

@interface SWRegisterViewController : SWViewController

@property (nonatomic, weak) IBOutlet UITextField *textFieldEmail;
@property (nonatomic, weak) IBOutlet UITextField *textFieldPassword;
@property (nonatomic, weak) IBOutlet UITextField *textFieldConfirmPassword;

@end
