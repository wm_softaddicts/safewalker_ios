//
//  SWScanViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 15.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface SWScanViewController : SWViewController <AVCaptureMetadataOutputObjectsDelegate, CLLocationManagerDelegate> {
    
    BOOL askedAboutPermission;
    float distance;
    
}

@property (nonatomic, weak) IBOutlet UIView *viewScan;

@property (nonatomic, strong) AVCaptureDevice *device;
@property (nonatomic, strong) AVCaptureDeviceInput *input;
@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureMetadataOutput *output;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *preview;


@property (nonatomic, strong) NSDictionary *data1;
@property (nonatomic, strong) NSDictionary *data2;
@property (nonatomic, strong) NSDate *date1;
@property (nonatomic, strong) NSDate *date2;
@property (nonatomic, strong) CLLocation *location1;
@property (nonatomic, strong) CLLocation *location2;

@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLLocation *tempOldLocation;

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;

@property (nonatomic, weak) IBOutlet UIView *viewDialog;
@property (nonatomic, weak) IBOutlet UIView *viewDialogCancel;
@property (nonatomic, weak) IBOutlet UIView *viewDialogScanFirst;
@property (nonatomic, weak) IBOutlet UIView *viewDialogScanSecond;
@property (nonatomic, weak) IBOutlet UILabel *labelDialogPoints;

@end
