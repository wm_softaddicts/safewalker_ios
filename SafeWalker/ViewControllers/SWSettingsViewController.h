//
//  SWSettingsViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 15.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"
#import <MessageUI/MessageUI.h>

@interface SWSettingsViewController : SWViewController <MFMailComposeViewControllerDelegate>

@end
