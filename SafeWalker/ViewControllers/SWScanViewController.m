//
//  SWScanViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 15.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWScanViewController.h"

@interface SWScanViewController ()

@end

@implementation SWScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _viewDialog.alpha = 0.0;
    _viewDialogCancel.layer.cornerRadius = 10;
    _viewDialogCancel.layer.masksToBounds = YES;

    _viewDialogScanFirst.layer.cornerRadius = 10;
    _viewDialogScanFirst.layer.masksToBounds = YES;
    
    _viewDialogScanSecond.layer.cornerRadius = 10;
    _viewDialogScanSecond.layer.masksToBounds = YES;
    
    self.data1 = nil;
    self.data2 = nil;
    
    if ([self isCameraAvailable]) {
    
        [self setupScanner];
    
    }
    
    self.currentLocation = nil;
    self.location1 = nil;
    self.location2 = nil;
    distance = 0.0;
    
    LManager.locationManager.delegate = self;
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self startScanning];
    
    [LManager.locationManager startUpdatingLocation];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self stopScanning];
    
    [LManager.locationManager stopUpdatingLocation];
    
}

#pragma mark -
#pragma mark CLLocation

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    if ([CLLocationManager locationServicesEnabled] && !askedAboutPermission){
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            askedAboutPermission = YES;
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:LS(@"App Permission Denied")
                                                               message:LS(@"To re-enable, please go to Settings and turn on Location Service for this app.")
                                                              delegate:nil
                                                     cancelButtonTitle:LS(@"Ok")
                                                     otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)locationManager: (CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    //[manager stopUpdatingLocation];
    
    if ([locations count]) {
        
        self.currentLocation = [locations lastObject];
        
        CLLocation *newLocation = [locations lastObject];
        
        if (newLocation.horizontalAccuracy < 0) return;
        if ((self.tempOldLocation == nil) || (self.tempOldLocation.horizontalAccuracy < 0)) {
            self.tempOldLocation = newLocation;
            distance = 0.0;
            
            return;
        }
        
        if (self.location1 && (self.location2 == nil))
        {
            
            distance = distance+([newLocation distanceFromLocation:self.tempOldLocation]);
            
        }
        
        self.tempOldLocation = newLocation;
        
    }

    
}

#pragma mark -
#pragma mark Dialogs

- (IBAction)hideDialog:(UIButton *)button {
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewDialog.alpha = 0.0;
        
    }];
    
}

- (void)showDialog:(UIView *)subview {
    
    for (UIView *viewDlg in [_viewDialog subviews]) {
        viewDlg.hidden = (viewDlg != subview);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewDialog.alpha = 1.0;
        
    }];
    
}

#pragma mark - Buttons

- (IBAction)buttonCancelPressed:(UIButton *)button {
    
    [self showDialog:_viewDialogCancel];
    
}

- (IBAction)buttonOkFirstPressed:(UIButton *)button {
    
    _labelTitle.text = LS(@"Waiting for second scan on the other side of the street.");
    
    [self hideDialog:nil];
    
    [self startScanning];
    
}

#pragma mark -
#pragma mark AVFoundationSetup

- (BOOL) isCameraAvailable;
{
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    return [videoDevices count] > 0;
}

- (void) setupScanner;
{
    self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    
    if (self.input) {
        
        self.session = [[AVCaptureSession alloc] init];
        
        self.output = [[AVCaptureMetadataOutput alloc] init];
        [self.session addOutput:self.output];
        [self.session addInput:self.input];
        
        [self.output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        self.output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
        
        self.preview = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
        self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
        self.preview.frame = CGRectMake(0, 0, self.viewScan.frame.size.width, self.viewScan.frame.size.height);
        
        //AVCaptureConnection *con = self.preview.connection;
        //con.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
        
        [self.viewScan.layer insertSublayer:self.preview atIndex:0];
        
    } else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"App Permission Denied")
                                                            message:LS(@"To re-enable, please go to Settings and turn on Camera for this app.")
                                                           delegate:nil
                                                  cancelButtonTitle:LS(@"Ok")
                                                  otherButtonTitles:nil];
        
        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }];
    }

}

#pragma mark -
#pragma mark AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection
{
    for(AVMetadataObject *current in metadataObjects) {
        if([current isKindOfClass:[AVMetadataMachineReadableCodeObject class]]) {
            NSString *scannedValue = [((AVMetadataMachineReadableCodeObject *) current) stringValue];
            [self didSuccessfullyScan:scannedValue];
        }
    }
}

- (void)startScanning;
{
    if (self.input) {
        [self.session startRunning];
    }
    
}

- (void) stopScanning;
{
    [self.session stopRunning];
}

- (void)didSuccessfullyScan:(NSString *)scannedValue {
    
    NSDictionary *data = [scannedValue JSONValue];
    if (data && [data isKindOfClass:[NSDictionary class]] && [data objectForKeyOrNil:@"id"]) {
        
        if (_data1 == nil) {
    
            NSLog(@"scannedValue %@", scannedValue);
            
            self.data1 = data;
            self.date1 = [NSDate date];
            self.location1 = self.currentLocation;
            distance = 0.0;
            
            [self stopScanning];
            
            [self showDialog:_viewDialogScanFirst];
            
        } else {
            
            if (![[[_data1 objectForKey:@"id"] stringValue] isEqualToString:[[data objectForKey:@"id"] stringValue]]) {
                
                
                NSLog(@"scannedValue %@", scannedValue);
                
                self.data2 = data;
                self.date2 = [NSDate date];
                self.location2 = self.currentLocation;
                
                [self stopScanning];
                
                [self showHUD];
                
                if (self.location1 == nil) self.location1 = [[CLLocation alloc] initWithLatitude:0.0 longitude:0.0];
                if (self.location2 == nil) self.location2 = [[CLLocation alloc] initWithLatitude:0.0 longitude:0.0];
                
                [CManager userScan:[[_data1 objectForKey:@"id"] stringValue] location1:_location1 time1:_date1 data2:[[_data2 objectForKey:@"id"] stringValue] location2:_location2 time2:_date2 dis:((int)distance) completionBlock:^(NSDictionary *result) {
                
                    if (result && [result objectForKeyOrNil:@"success"]) {
                        
                        [CManager userProfile:^(NSDictionary *resultProfile) {
                            
                            [self hideHUD];
                            
                            _labelDialogPoints.text = [[result objectForKeyOrNil:@"success"] stringValue];
                            [self showDialog:_viewDialogScanSecond];
                            
                        }];
                        
                        
                    } else {
                        
                        
                        [self hideHUD];
                        
                        if ([result objectForKey:@"error"]) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                            [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                
                                if (buttonIndex == 0) {
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                }
                                
                                
                            }];
                        }
                        
                        if (result == nil) {
                            
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"Your device is experiencing network issues. Please try again.") delegate:nil cancelButtonTitle:LS(@"Cancel") otherButtonTitles:LS(@"Try again"), nil];
                            [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                
                                if (buttonIndex == 0) {
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                } else if (buttonIndex == 1) {
                                    
                                    [self startScanning];
                                    
    
                                }
                                
                                
                            }];
                            
                        }
                        
                        
                    }
                    
                }];
                
                
            }
            
        }
        
    }
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
