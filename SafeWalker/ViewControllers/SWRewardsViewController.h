//
//  SWRewardsViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"

@interface SWRewardsViewController : SWViewController {
    
    int selectedType;
    BOOL loadingInProgress;
    
}

@property (nonatomic, strong) SWCoupon *selectedCoupon;

@property (nonatomic, strong) IBOutlet UIView *viewSegmentsBar;

@property (nonatomic, weak) IBOutlet UITableView *tableViewRewards;

@property (nonatomic, weak) IBOutlet UIView *viewInfo;
@property (nonatomic, weak) IBOutlet UIView *viewInfoRounded;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewInfoContent;
@property (nonatomic, weak) IBOutlet UIView *viewInfoContent;

@property (nonatomic, weak) IBOutlet UILabel *labelInfoTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imageInfo;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoExpires;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoVoucher;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoBarCode;
@property (nonatomic, weak) IBOutlet UIButton *buttonRedeem;
@property (nonatomic, weak) IBOutlet UIButton *buttonCancel;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoDescription;

@property (nonatomic, weak) IBOutlet UILabel *labelNoData;

@end
