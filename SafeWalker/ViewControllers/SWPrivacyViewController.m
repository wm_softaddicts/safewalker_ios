//
//  SWPrivacyViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWPrivacyViewController.h"

@interface SWPrivacyViewController ()

@end

@implementation SWPrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *strURL = [DManager.profile objectForKeyOrNil:@"privacy_policy_url"];
    if (_helpType) {
        strURL = [DManager.profile objectForKeyOrNil:@"help_url"];
     }
    
    if (strURL) {
        [_webViewContent loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strURL]]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
