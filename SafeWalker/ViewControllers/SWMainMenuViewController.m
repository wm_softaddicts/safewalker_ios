//
//  SWMainMenuViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 15.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWMainMenuViewController.h"
#import "SWPrivacyViewController.h"

@interface SWMainMenuViewController ()

@end

@implementation SWMainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (!DManager.profile) {
        
        DManager.user_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
        DManager.user_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_token"];
    
        super.labelHeader.hidden = YES;
        
        [self showHUD];
        
        [CManager userProfile:^(NSDictionary *result) {
            
            super.labelHeader.hidden = NO;
            [self hideHUD];
            
        }];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue destinationViewController] isKindOfClass:[SWPrivacyViewController class]]) {
        SWPrivacyViewController *privacyController = [segue destinationViewController];
        privacyController.helpType = YES;
    }
    
}

@end
