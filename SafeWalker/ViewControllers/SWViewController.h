//
//  SWViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 11.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface SWViewController : UIViewController <MBProgressHUDDelegate> {
    
    MBProgressHUD *HUD;
    
    BOOL isInitialized;
    
}

- (void)showHUD;
- (void)hideHUD;
- (void)closeKeyboard;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewContent;
@property (nonatomic, weak) IBOutlet UIView *viewContent;
@property (nonatomic, weak) IBOutlet UILabel *labelHeader;
@property (nonatomic, weak) IBOutlet UILabel *labelFooter;

@end
