//
//  SWShopTableViewCell.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 19.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWShopTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewContent;
@property (nonatomic, weak) IBOutlet UIImageView *imgCoupon;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelExpire;
@property (nonatomic, weak) IBOutlet UIButton *buttonShop;

@end
