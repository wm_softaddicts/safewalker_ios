//
//  SWShopTableViewCell.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 19.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWShopTableViewCell.h"

@implementation SWShopTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _viewContent.layer.cornerRadius = 10;
    _viewContent.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _viewContent.backgroundColor = highlighted ? [UIColor colorWithWhite:0.9 alpha:1.0] : [UIColor whiteColor];

}

@end
