//
//  SWLoginViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWLoginViewController.h"
#import "SWMainMenuViewController.h"

@interface SWLoginViewController ()

@end

@implementation SWLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}


#pragma mark - UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _textFieldEmail) {
        [_textFieldPassword becomeFirstResponder];
    } else {
        [self buttonLoginPressed:nil];
    }
    
    return YES;
    
}

- (void)closeKeyboard {

    [_textFieldEmail resignFirstResponder];
    [_textFieldPassword resignFirstResponder];
    
}

- (IBAction)buttonLoginPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    
    NSString *strError = @"";
    
    if ([_textFieldEmail.text length] == 0 || [_textFieldPassword.text length] == 0) {
        
        strError = LS(@"Please fill all field");
        
    }
    
    if ([strError length] == 0 && ![Utils validateEmail:_textFieldEmail.text]) {
        
        strError = LS(@"Incorrect email format");
        
    }
    
    if ([strError length] == 0) {
        
        [self showHUD];

        [CManager login:_textFieldEmail.text password:_textFieldPassword.text completionBlock:^(NSDictionary *result) {
        
            [self hideHUD];
            
            if (!result || [result objectForKeyOrNil:@"error"]) {
                
                if ([result objectForKey:@"error"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                    [alert show];
                }
                
            } else if (DManager.user_token) {
                
                SWMainMenuViewController *controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"SWMainMenuViewController"];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
        }];
        
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strError message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
        [alert show];
        
    }
    

    
}

- (IBAction)buttonForgotPasswordPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    NSString *strError = @"";
    
    if ([_textFieldEmail.text length] == 0) {
        
        strError = LS(@"Please fill email field");
        
    }
    
    if (![Utils validateEmail:_textFieldEmail.text]) {
        
        strError = LS(@"Incorrect email format");
        
    }
    
    if ([strError length] == 0) {
        
        [self showHUD];
        
        [CManager remindPassword:_textFieldEmail.text completionBlock:^(NSDictionary *result) {
            
            [self hideHUD];
            
            if (!result || [result objectForKeyOrNil:@"error"]) {
                
                if ([result objectForKey:@"error"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                    [alert show];
                }
                
            } else if ([result objectForKeyOrNil:@"success"]) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"success"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                [alert show];
                
            }
            
        }];
        
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strError message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
