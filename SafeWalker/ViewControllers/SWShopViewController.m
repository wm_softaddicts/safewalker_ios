//
//  SWShopViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWShopViewController.h"
#import "SWShopTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SWShopViewController ()

@end

@implementation SWShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _viewInfo.alpha = 0.0;
    _viewInfoContentBuy.layer.cornerRadius = 10;
    _viewInfoContentBuy.layer.masksToBounds = YES;
    
    _viewInfoRounded.layer.cornerRadius = 10;
    _viewInfoRounded.layer.masksToBounds = YES;
    
    _tableViewShop.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableViewShop.frame.size.width, 4.0)];
    _tableViewShop.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableViewShop.frame.size.width, 4.0)];
    
    [self showHUD];
    
    [DManager.shop removeAllObjects];
    DManager.shopPages = 0;
    DManager.shopPage = 0;
    
    [_tableViewShop reloadData];
    
    [CManager updateShop:DManager.shopPage completionBlock:^(NSDictionary *result) {
        
        DManager.shopPage++;
        DManager.shopPages = [[result objectForKeyOrNil:@"pages"] integerValue];
        
        if ([result objectForKeyOrNil:@"coupons"]) {
            for (NSDictionary *coupon in [result objectForKey:@"coupons"]) {
                
                [DManager.shop addObject:[SWCoupon couponWithDictionary:coupon]];
            }
        }
        
        [self hideHUD];
        
        [_tableViewShop reloadData];
        
    }];
    
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ((scrollView == _tableViewShop) && !loadingInProgress && (DManager.shopPage < DManager.shopPages)) {
    
        float pageHeight = 20 * _tableViewShop.rowHeight;
        
        float maxOffset = (DManager.shopPage - 1) * pageHeight + (pageHeight / 2);
        
        if (scrollView.contentOffset.y > maxOffset) {
            loadingInProgress = YES;
            
            [CManager updateShop:DManager.shopPage completionBlock:^(NSDictionary *result) {
                
                DManager.shopPage++;
                
                if ([result objectForKeyOrNil:@"coupons"]) {
                    for (NSDictionary *coupon in [result objectForKey:@"coupons"]) {
                        
                        [DManager.shop addObject:[SWCoupon couponWithDictionary:coupon]];
                    }
                }
                
                [_tableViewShop reloadData];
                loadingInProgress = NO;
                
            }];
            
        }
        
    }
    
}

#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [DManager.shop count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId = @"SWShopTableViewCell";
    SWShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SWShopTableViewCell" owner:tableView options:nil] objectAtIndex:([Utils isiPhone] ? 0 : 1)];
    }
    
    SWCoupon *coupon = [DManager.shop objectAtIndex:indexPath.row];
    
    cell.imgCoupon.image = nil;
    
    if (coupon.img) [cell.imgCoupon sd_setImageWithURL:[NSURL URLWithString:coupon.img]];
    
    cell.labelTitle.text = coupon.title;
    cell.labelExpire.text = [NSString stringWithFormat:@"%@ %@", LS(@"Expires"), coupon.expire_date];
    
    [cell.buttonShop setTitle:[NSString stringWithFormat:LS(@"Get for %@ points"), [coupon.price stringValue]] forState:UIControlStateNormal];
    cell.buttonShop.enabled = ([coupon.price integerValue] <= [DManager.userPoints integerValue]);
    cell.buttonShop.tag = indexPath.row;
    [cell.buttonShop addTarget:self action:@selector(buttonInfoPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    cell.selectedBackgroundView = [UIView new];
    
    return cell;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.selectedCoupon = [DManager.shop objectAtIndex:indexPath.row];
    
    _imageInfo.image = nil;
    
    if (_selectedCoupon.img) [_imageInfo sd_setImageWithURL:[NSURL URLWithString:_selectedCoupon.img]];
    
    _labelInfoTitle.text = _selectedCoupon.title;
    _labelInfoExpires.text = [NSString stringWithFormat:@"%@: %@", LS(@"Expires"), _selectedCoupon.expire_date];
    int titleWidth = [LS(@"Expires") length] + 2;
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:_labelInfoExpires.text];
    
    [attrStr addAttribute:NSForegroundColorAttributeName value:_labelInfoExpires.textColor range:NSMakeRange(0, attrStr.length)];
    [attrStr addAttribute:NSFontAttributeName value:_labelInfoExpires.font range:NSMakeRange(0, attrStr.length)];
    
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.0 green:0.56 blue:0.0 alpha:1.0] range:NSMakeRange(titleWidth, [_labelInfoExpires.text length] - titleWidth)];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:_labelInfoExpires.font.pointSize] range:NSMakeRange(titleWidth, [_labelInfoExpires.text length] - titleWidth)];
    
    _labelInfoExpires.attributedText = attrStr;

    [_buttonBuy setTitle:[NSString stringWithFormat:LS(@"Get for %@ points"), [_selectedCoupon.price stringValue]] forState:UIControlStateNormal];
    _buttonBuy.enabled = ([_selectedCoupon.price integerValue] <= [DManager.userPoints integerValue]);
    
    _labelInfoDescription.text = _selectedCoupon.couponDescription;
    
    NSDictionary *attributes = @{ NSFontAttributeName: _labelInfoDescription.font };
    
    CGRect size = [_labelInfoDescription.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(_labelInfoDescription.frame), MAXFLOAT)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil];
    _labelInfoDescription.frame = CGRectMake(_labelInfoDescription.frame.origin.x, _labelInfoDescription.frame.origin.y, _labelInfoDescription.frame.size.width, size.size.height + 5.0);
    _viewInfoContent.frame = CGRectMake(0, 0, _viewInfoContent.frame.size.width, _labelInfoDescription.frame.origin.y + _labelInfoDescription.frame.size.height + 10.0);
    _scrollViewInfoContent.contentSize = _viewInfoContent.frame.size;
    _scrollViewInfoContent.contentOffset = CGPointZero;
    
    _viewInfoRounded.hidden = NO;
    _viewInfoContentBuy.hidden = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewInfo.alpha = 1.0;
        
    }];

    
}

#pragma mark - Buttons

- (void)buttonInfoPressed:(UIButton *)button {
    
    if (button.tag < [DManager.shop count]) {
        
        self.selectedCoupon = [DManager.shop objectAtIndex:button.tag];
    
        _labelInfoTitleBuy.text = _selectedCoupon.title;
        _labelInfoExpiresBuy.text = [NSString stringWithFormat:@"%@ %@", LS(@"Expires"), _selectedCoupon.expire_date];
        
        NSString *headerBegin = LS(@"Buy for ");
        NSString *headerPoints = [_selectedCoupon.price stringValue];
        NSString *headerEnd = LS(@" points");
        
        
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", headerBegin, headerPoints, headerEnd]];
        
        [attrStr addAttribute:NSForegroundColorAttributeName value:_labelInfoPriceBuy.textColor range:NSMakeRange(0, attrStr.length)];
        [attrStr addAttribute:NSFontAttributeName value:_labelInfoPriceBuy.font range:NSMakeRange(0, attrStr.length)];
        
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:_labelInfoPriceBuy.font.fontName size:_labelInfoPriceBuy.font.pointSize + 3.0] range:NSMakeRange(headerBegin.length, headerPoints.length)];
        
        _labelInfoPriceBuy.attributedText = attrStr;

        _viewInfoRounded.hidden = YES;
        _viewInfoContentBuy.hidden = NO;
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewInfo.alpha = 1.0;
            
        }];
        
    }
}

- (IBAction)buttonBuyCancelPressed:(UIButton *)button {
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewInfo.alpha = 0.0;
        
    }];
}

- (IBAction)buttonBackPressed:(UIButton *)button {
    
    if (_viewInfo.alpha > 0) {
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewInfo.alpha = 0.0;
            
        }];
        
    } else {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (IBAction)buttonBuyPressed:(UIButton *)button {
    
    if (_selectedCoupon) {
        
        [self showHUD];
        
        [CManager userBuy:_selectedCoupon.id completionBlock:^(NSDictionary *result) {
        
            [self hideHUD];
            
            [_tableViewShop reloadData];
            
            [UIView animateWithDuration:0.3 animations:^{
                _viewInfo.alpha = 0.0;
                
            }];
            
            if (!result || [result objectForKeyOrNil:@"error"]) {
                
                if ([result objectForKey:@"error"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[result objectForKey:@"error"] message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                    [alert show];
                }
            
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"Coupon was added to your rewards") message:nil delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
                [alert show];
                
            }
            
        }];
        
        
    }
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
