//
//  SWViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 11.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"

@interface SWViewController ()

@end

@implementation SWViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_viewContent) {
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
        [_viewContent addGestureRecognizer:recognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)updateHeaderAndFooter {
    
    if (_labelHeader) {

        if (DManager.userPoints) {
            
            NSString *headerBegin = LS(@"You have: ");
            NSString *headerPoints = [DManager.userPoints stringValue];
            NSString *headerEnd = LS(@" points");
            
            
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", headerBegin, headerPoints, headerEnd]];
            
            [attrStr addAttribute:NSForegroundColorAttributeName value:_labelHeader.textColor range:NSMakeRange(0, attrStr.length)];
            [attrStr addAttribute:NSFontAttributeName value:_labelHeader.font range:NSMakeRange(0, attrStr.length)];
            
            [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:_labelHeader.font.fontName size:_labelHeader.font.pointSize + 3.0] range:NSMakeRange(headerBegin.length, headerPoints.length)];
            
            _labelHeader.attributedText = attrStr;
            
        } else {
            
            _labelHeader.text = @"";
            
        }
        
        
    }

    if (_labelFooter) {
    
        if (DManager.userMail) {
            _labelFooter.text = [NSString stringWithFormat:@"%@: %@", LS(@"User"), DManager.userMail];
        } else {
            _labelFooter.text = @"";
        }
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    if (!isInitialized) {
        
        if (_viewContent && _scrollViewContent) {
            
            _viewContent.frame = _scrollViewContent.bounds;
            _scrollViewContent.contentSize = _viewContent.frame.size;
            
        }
        
        isInitialized = YES;
        
    }
    
    [self updateHeaderAndFooter];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateHeaderAndFooter)
                                                 name:SWUserProfileUpdatedNotification
                                               object:nil];
    
    
}
- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:SWUserProfileUpdatedNotification object:nil];

}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    if (_scrollViewContent) {
        
        float newHeight = [_scrollViewContent superview].frame.size.height - _scrollViewContent.frame.origin.y - kbSize.height;
        
        [UIView animateWithDuration:0.35 animations:^{
            _scrollViewContent.frame = CGRectMake(_scrollViewContent.frame.origin.x, _scrollViewContent.frame.origin.y, _scrollViewContent.frame.size.width, newHeight);
        }];
        
    }

}

- (void)keyboardWillHide:(NSNotification*)notification {
    
    if (_scrollViewContent) {
        
        float newHeight = [_scrollViewContent superview].frame.size.height - _scrollViewContent.frame.origin.y;
        
        [UIView animateWithDuration:0.35 animations:^{
            _scrollViewContent.frame = CGRectMake(_scrollViewContent.frame.origin.x, _scrollViewContent.frame.origin.y, _scrollViewContent.frame.size.width, newHeight);
        }];
        
    }

}

#pragma mark - UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)closeKeyboard {
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    [self closeKeyboard];
    
}

/*
- (void)textFieldDidBeginEditing:(UITextField *)textField {

    if (_scrollViewContent) {
        
        [_scrollViewContent scrollRectToVisible:[textField superview].frame animated:YES];
        
    }
    
}
*/
#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)showHUD {
    // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
    
    if (HUD != nil) return;
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
    
}

- (void)hideHUD {
    
    [HUD hide:YES];
    
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}


#pragma mark - Navigation

- (IBAction)buttonBackPressed:(UIButton *)button {

    [self.navigationController popViewControllerAnimated:YES];
    
}

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
