//
//  SWPrivacyViewController.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWViewController.h"

@interface SWPrivacyViewController : SWViewController

@property (nonatomic, weak) IBOutlet UIWebView *webViewContent;

@property BOOL helpType;

@end
