//
//  ViewController.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 11.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "ViewController.h"
#import "SWMainMenuViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if ([LManager.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [LManager.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
    }
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"user_token"]) {
        
        SWMainMenuViewController *controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"SWMainMenuViewController"];
        [self.navigationController pushViewController:controller animated:NO];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
