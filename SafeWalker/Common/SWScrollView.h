//
//  SWScrollView.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 19.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWScrollView : UIScrollView

@end
