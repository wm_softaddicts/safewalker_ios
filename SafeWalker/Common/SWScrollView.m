//
//  SWScrollView.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 19.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWScrollView.h"

@implementation SWScrollView

- (UIView *) hitTest:(CGPoint) point withEvent:(UIEvent *)event {
    if ([self pointInside:point withEvent:event]) {
        if ([[self subviews] count] > 0) {
            return [[self subviews] objectAtIndex:0];
        }
    }
    return nil;
}

@end
