//
//  Utils.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 17.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSDictionary+Additions.h"
#import "NSDate+Utilities.h"
#import "UIAlerView+Blocks.h"
#import "SBJson.h"

#import "SWCoupon.h"

@interface Utils : NSObject

+ (BOOL)validateEmail:(NSString *)candidate;
+ (NSString *)encodeString:(NSString *)string;
+ (NSString *)urlEncode:(NSString *)str;
+ (BOOL)isiPhone;

@end
