//
//  SWCoupon.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 20.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWCoupon.h"

@implementation SWCoupon

+ (SWCoupon *)couponWithDictionary:(NSDictionary *)object {
    
    SWCoupon *item = [[self alloc] init];
    [item configureWithDictionary:object];
    
    return item;
}

- (void)configureWithDictionary:(NSDictionary *)object {
    
    self.couponDescription = [object objectForKeyOrNil:@"description"];
    self.expire_date = [object objectForKeyOrNil:@"expire_date"];
    self.id = [[object objectForKeyOrNil:@"id"] stringValue];
    self.img = [object objectForKeyOrNil:@"img"];
    self.price = [NSNumber numberWithInteger:[[object objectForKeyOrNil:@"price"] integerValue]];
    self.title = [object objectForKeyOrNil:@"title"];
    if ([object objectForKeyOrNil:@"number"]) {
        self.number = [[object objectForKeyOrNil:@"number"] stringValue];
    }
    
}


@end
