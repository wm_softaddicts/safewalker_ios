//
//  SWCoupon.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 20.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWCoupon : NSObject

+ (SWCoupon *)couponWithDictionary:(NSDictionary *)object;

@property (nonatomic, strong) NSString *couponDescription;
@property (nonatomic, strong) NSString *expire_date;
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *img;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSString *title;

@end
