//
//  SWConnectManager.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWConnectManager.h"
#import "NSString+AESCrypt.h"


NSString * const SWUserProfileUpdatedNotification = @"SWUserProfileUpdatedNotification";

@implementation SWConnectManager

+ (SWConnectManager *)instance {
    
    static SWConnectManager *instance = nil;
    @synchronized (self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] initWithBaseURL:[NSURL URLWithString:SW_SERVER_URL]];
            
            instance.responseSerializer = [AFHTTPResponseSerializer serializer];
            
        }
    }
    return instance;
}


- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self)
    {
        
        
    }
    return self;
}

- (void)saveToken:(NSDictionary *)result {
    
    DManager.user_id = [result objectForKeyOrNil:@"user_id"];
    DManager.user_token = [result objectForKeyOrNil:@"user_token"];
    
    if (DManager.user_token) {
        
        [[NSUserDefaults standardUserDefaults] setObject:DManager.user_id forKey:@"user_id"];
        [[NSUserDefaults standardUserDefaults] setObject:DManager.user_token forKey:@"user_token"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

- (void)showErrorAlert {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LS(@"Your device is experiencing network issues. Please try again.") delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
    [alert show];
    
}

- (void)login:(NSString *)email password:(NSString *)password completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:email, @"email", password, @"password", nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@userLogin?data=%@", SW_SERVER_URL, ES(data)];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        [self saveToken:result];
        
        //NSLog(@"Response login: %@", [result description]);
        
        if (DManager.user_token) {
            
            [self userProfile:^(NSDictionary *result) {
                completionBlock(result);
            }];
            
        } else {
            completionBlock(result);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)signup:(NSString *)email password:(NSString *)password completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:email, @"email", password, @"password", nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@userSignup?data=%@", SW_SERVER_URL, ES(data)];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        [self saveToken:result];
        
        //NSLog(@"Response signup: %@", [result description]);
        
        if (DManager.user_token) {
            
            [self userProfile:^(NSDictionary *result) {
                completionBlock(result);
            }];
            
        } else {
            completionBlock(result);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)remindPassword:(NSString *)email completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@userRemindPassword?mail=%@", SW_SERVER_URL, email];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response remindPassword: %@", [result description]);
        
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}


- (void)changePassword:(NSString *)newPassword password:(NSString *)password completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:newPassword, @"newpassword", password, @"password", nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@userChangePassword?user=%@&token=%@&data=%@", SW_SERVER_URL, DManager.user_id, DManager.user_token, ES(data)];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response changePassword: %@", [result description]);
        
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)userProfile:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@userProfile?user=%@&token=%@", SW_SERVER_URL, DManager.user_id, DManager.user_token];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response userProfile: %@", [result description]);
        
        if ([result objectForKeyOrNil:@"user_points"]) {
            
            DManager.profile = result;
            DManager.userMail = [result objectForKeyOrNil:@"user_mail"];
            DManager.userPoints = [NSNumber numberWithInteger:[[result objectForKey:@"user_points"] integerValue]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:SWUserProfileUpdatedNotification object:nil];
            
        }
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)updateShop:(int)page completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@getShop?user=%@&token=%@&page=%d", SW_SERVER_URL, DManager.user_id, DManager.user_token, page];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response updateShop: %@", [result description]);
        
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)userBuy:(NSString *)couponId completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@userBuy?user=%@&token=%@&id=%@", SW_SERVER_URL, DManager.user_id, DManager.user_token, couponId];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response userBuy: %@", [result description]);
        
        [self userProfile:^(NSDictionary *resultProfile) {
            
            completionBlock(result);
            
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)userRedeem:(NSString *)couponId completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@userRedeem?user=%@&token=%@&id=%@", SW_SERVER_URL, DManager.user_id, DManager.user_token, couponId];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response userRedeem: %@", [result description]);
        
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)getCoupon:(NSString *)couponId completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@getCoupon?user=%@&token=%@&id=%@", SW_SERVER_URL, DManager.user_id, DManager.user_token, couponId];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response getCoupon: %@", [result description]);
        
        [self userProfile:^(NSDictionary *resultProfile) {
            
            completionBlock(result);
            
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)updateRewards:(NSString *)type page:(int)page completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@userPurchases?user=%@&token=%@&type=%@&page=%d", SW_SERVER_URL, DManager.user_id, DManager.user_token, type, page];
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response updateRewards: %@", [result description]);
        
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}

- (void)userScan:(NSString *)data1 location1:(CLLocation *)location1 time1:(NSDate *)time1 data2:(NSString *)data2 location2:(CLLocation *)location2 time2:(NSDate *)time2 dis:(int)dis completionBlock:(void(^)(NSDictionary *result))completionBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@userScan?user=%@&token=%@&data1=%@&lat1=%f&long1=%f&time1=%@&data2=%@&lat2=%f&long2=%f&time2=%@&dis=%d", SW_SERVER_URL, DManager.user_id, DManager.user_token, data1, location1.coordinate.latitude, location1.coordinate.longitude, [[NSNumber numberWithLongLong:[time1 timeIntervalSince1970] * 1000] stringValue], data2, location2.coordinate.latitude, location2.coordinate.longitude, [[NSNumber numberWithLongLong:[time2 timeIntervalSince1970] * 1000] stringValue], dis];
    
    //NSLog(urlString);
    
    [self GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        //NSLog(@"Response userScan: %@", [result description]);
        
        completionBlock(result);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //[self showErrorAlert];
        completionBlock(nil);
        
    }];
    
}


- (void)logout {
    
    DManager.user_id = nil;
    DManager.user_token = nil;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

@end
