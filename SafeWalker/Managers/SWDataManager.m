//
//  SWDataManager.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWDataManager.h"

@implementation SWDataManager

+ (SWDataManager *)instance {
    
    static SWDataManager *instance = nil;
    @synchronized (self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] init];
            
        }
    }
    return instance;
}


- (id)init {
    self = [super init];
    if (self)
    {
        self.shop = [[NSMutableArray alloc] init];
        self.rewards = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSString *)textForKey:(NSString *)key {
    
    return key;
    
}


@end
