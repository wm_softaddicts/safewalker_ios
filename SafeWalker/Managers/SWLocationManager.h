//
//  SWLocationManager.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 23.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWLocationManager : NSObject <CLLocationManagerDelegate>

+ (SWLocationManager *)instance;

@property (nonatomic, strong) CLLocationManager *locationManager;

@end
