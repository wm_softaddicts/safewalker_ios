//
//  SWConnectManager.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

extern NSString * const SWUserProfileUpdatedNotification;

@interface SWConnectManager : AFHTTPRequestOperationManager

+ (SWConnectManager *)instance;

- (void)login:(NSString *)email password:(NSString *)password completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)signup:(NSString *)email password:(NSString *)password completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)remindPassword:(NSString *)email completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)changePassword:(NSString *)newPassword password:(NSString *)password completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)userProfile:(void(^)(NSDictionary *result))completionBlock;

- (void)updateShop:(int)page completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)userBuy:(NSString *)couponId completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)userRedeem:(NSString *)couponId completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)getCoupon:(NSString *)couponId completionBlock:(void(^)(NSDictionary *result))completionBlock;
- (void)updateRewards:(NSString *)type page:(int)page completionBlock:(void(^)(NSDictionary *result))completionBlock;

- (void)userScan:(NSString *)data1 location1:(CLLocation *)location1 time1:(NSDate *)time1 data2:(NSString *)data2 location2:(CLLocation *)location2 time2:(NSDate *)time2 dis:(int)dis completionBlock:(void(^)(NSDictionary *result))completionBlock;

- (void)logout;

@end
