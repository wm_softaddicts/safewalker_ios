//
//  SWDataManager.h
//  SafeWalker
//
//  Created by Pavel Belevtsev on 18.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWDataManager : NSObject

+ (SWDataManager *)instance;

- (NSString *)textForKey:(NSString *)key;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *user_token;

@property (nonatomic, strong) NSMutableArray *shop;
@property (nonatomic) NSInteger shopPages;
@property (nonatomic) NSInteger shopPage;

@property (nonatomic, strong) NSMutableArray *rewards;
@property (nonatomic) NSInteger rewardsPages;
@property (nonatomic) NSInteger rewardsPage;

@property (nonatomic, strong) NSDictionary *profile;

@property (nonatomic, strong) NSString *userMail;
@property (nonatomic, strong) NSNumber *userPoints;

@end
