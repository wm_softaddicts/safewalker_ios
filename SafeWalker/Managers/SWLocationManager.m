//
//  SWLocationManager.m
//  SafeWalker
//
//  Created by Pavel Belevtsev on 23.03.15.
//  Copyright (c) 2015 Softaddicts. All rights reserved.
//

#import "SWLocationManager.h"

@implementation SWLocationManager

+ (SWLocationManager *)instance {
    
    static SWLocationManager *instance = nil;
    @synchronized (self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] init];
        }
    }
    return instance;
    
}

- (id)init {
    self = [super init];
    if (self)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
    }
    return self;
}

@end
